﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(BigSchool_A4.Startup))]
namespace BigSchool_A4
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
